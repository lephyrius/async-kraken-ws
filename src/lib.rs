//! 
//! This is a minimal interface for the [Kraken exchange Websockets API](https://docs.kraken.com/websockets/) using the [async-std](https://async.rs/) runtime.
//! 
//! This crate will handle the auth token adquisition and automatically add it to all private messages send to Kraken.
//! 
//! # Prerequisites
//! To use the **```private```** connection you will need to generate an **```API-Key```** and an **```API-Secret```** to authenticate to the desired Kraken account.  
//! [How to generate an API key pair?](https://support.kraken.com/hc/en-us/articles/360000919966-How-to-generate-an-API-key-pair-)
//! 
//! # Usage
//! Private and public channels can't be mixed, so you will need to create a ``KrakenWSHandler`` for each type.  
//! 
//! To create a handler for the public channels use ``KrakenWS::run_public``, and for a private handler use ``KrakenWS::run_private``.  
//! 
//! Apart from the ``key`` and ``secret`` parameters for the private handler, both methods require a callback having a [Value](https://docs.serde.rs/serde_json/value/enum.Value.html) JSON object as input. This **callback** will be used when receiving messages from Kraken for the data to be handled. 
//! 
//! The base subscribable channels have their own methods, but they can be bypassed using ``send_private`` or ``send_public`` plus a [Value](https://docs.serde.rs/serde_json/value/enum.Value.html) payload. Remember that the auth token is added automatically to all private messages.
//! 
//! Read the [Kraken documentation](https://docs.kraken.com/websockets/) to understand the messages.
//! 
//! # Example
//! ```rust
//! use async_kraken_ws::client::KrakenWS;
//! 
//! fn get_keys() -> (String, String) {
//!     let content = std::fs::read_to_string("key").expect("File not found");
//!     let lines: Vec<&str> = content.lines().collect();
//! 
//!     let key = String::from(lines[0]);
//!     let secret = String::from(lines[1]);
//! 
//!     (key, secret)
//! }
//! 
//! #[async_std::main]
//! async fn main() {
//!     let (api_key, api_secret) = get_keys();
//!     let kpr = KrakenWS::run_private(|x| println!("{}", x.to_string()), api_key, api_secret).await;
//!     let _ = kpr.subscribe_open_orders().await;
//!     let _ = kpr.subscribe_own_trades().await;
//! 
//!     let kpu = KrakenWS::run_public(|x| println!("{}", x.to_string())).await;
//!     let _ = kpu.subscribe_ticker(vec!["XBT/EUR"]).await;
//!     let _ = kpu.subscribe_ohlc(vec!["XBT/EUR"], 1).await;
//!     let _ = kpu.subscribe_book(vec!["XBT/EUR"], 100).await;
//!     let _ = kpu.subscribe_spread(vec!["XBT/EUR"]).await;
//!     let _ = kpu.subscribe_trade(vec!["XBT/EUR"]).await;
//! 
//!     loop {
//!         std::thread::sleep(std::time::Duration::from_secs(30));
//!         let _ = kpu.ping().await;
//!         let _ = kpr.ping().await;
//!     }
//! }
//! 
//! ```
//! 
//! # Disclaimer
//! This software comes without any kind of warranties.  
//! You are the sole responsible of your gains or loses.
//! 

pub mod client;
mod config;
