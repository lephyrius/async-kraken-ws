pub const API_URL_PUBLIC: &str = "wss://ws.kraken.com";
pub const API_URL_PRIVATE: &str = "wss://ws-auth.kraken.com";
